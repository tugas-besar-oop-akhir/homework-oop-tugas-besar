# No.1
Saya tidak paham di bagian konsep encapsulation dan polymorphism di konsep bagian dua itu saya masih
mencari source code untuk project saya

# No.2





# No.3
 1.Absstraction
 Absstraction adalah suatu prinsip dimana pengembang bisa menyuruh suatu fungsi tidak perlu mengetahui kinerja dari fungsi tersebut.Dengan kata lain absstraction dapat menampilkan informasi yang di perlukan saja terlebih dahulu
 2.Inheritance
 Inheritance adalah suatu pewarisan data class ke data class data yang baru tujuan dari konsepram yang inheritance adalah untuk memanfaatkan fitur code reuse yang mencegah data class atau kode program yang bersifat duplikasi
 3.Encapsulation
 encapsulation atau enkapsulasi adalah pembungkusan data atau penyembunyian class atau data yang bersifat privat.Hal ini bertujuan agar dapat menyembunyikan data-data privat sehingga tidak dapat di akses oleh obyek lain.
 4.Polymorphism



# No.4









# No.5
``````java
public class gameplay_genshin {
    public static void main(String[] args) {
        gameplay_genshin genshin = new gameplay_genshin();
        genshin.bergerak();
        genshin.menyerang();
        genshin.fitur_karakter();
        genshin.map();
    }

    void bergerak() {
        String bergerak1 = "Maju";
        String bergerak2 = "Mundur";
        String bergerak3 = "Belok kanan";
        String bergerak4 = "Belok kiri";
        String bergerak5 = "Terbang Menggunakan wind glider";
        String bergerak6 = "Gerakan Animasi";
        System.out.println("======BERGERAK=====");
        System.out.println("1." + bergerak1);
        System.out.println("2." + bergerak2);
        System.out.println("3." + bergerak3);
        System.out.println("4." + bergerak4);
        System.out.println("5." + bergerak5);
        System.out.println("6." + bergerak6);
    }

    void menyerang() {
        String menyerang1 = "Memukul";
        String menyerang2 = "Memakai Skill Elemental Vision";
        String menyerang3 = "Energy dari Pemakaian Skill Penuh Untuk Burst";
        String menyerang4 = "Menggunakan Burst dari Energy yang Penuh";
        System.out.println("=====MENYERANG DI GENSHIN=====");
        System.out.println("1." + menyerang1);
        System.out.println("2." + menyerang2);
        System.out.println("3." + menyerang3);
        System.out.println("4." + menyerang4);
    }

    void fitur_karakter() {
        System.out.println("=====FITUR KARAKTER====");
        System.out.println("1.Masuk ke dalam Pengaturan Party");
        System.out.println("2.Bisa Mengganti karakter dalam 4 Tim");
        System.out.println("3.Masuk ke dalam List Karakter-karakter");
        System.out.println("4.Menampilkan Stats,Senjata,Artefak,Konstelasi,Talenta,Biodata Karakter");
        System.out.println("5.Menampilkan Jumlah Karakter yang punya dan sesuai elemental Vision nya");
    }

    void map() {
        String map1 = "Mondstadt : Mondstadt City,Stormterrolair,Dragonspine,";
        String map2 = "Liyue : Liyue Harbour,The Chasm,Minlin,Lisha,Sea Of Cloud,Bishui Plain,Qiongli Estualy";
        String map3 = "Inazuma : Narukami Island,Kannazuka,Yashiori Island,Seirai Island,Watatsumi Island,Tsurumi Island";
        String map4 = "Sumeru : Avidya Forest,Vissudha Field,Ashavan Realm,Ardravi Valley,Land of Lower Setekh,Hypostyle Desert,Desert of Hadramaveth";
        System.out.println("=====MAP=====");
        System.out.println("1." + map1);
        System.out.println("2." + map2);
        System.out.println("3." + map3);
        System.out.println("4." + map4);
    }
}
# No.6
Inheritance
````java
class Gameplay {
  String maju;
  String mundur;
  String attack;
  String loncat;
}

class Ingame extends Gameplay {
  Ingame(String paramMaju, String paramMundur, String paramAttack, String paramLoncat) {
    maju=paramMaju;
    mundur=paramMundur;
    attack=paramAttack;
    loncat=paramLoncat;
  }

   String lihatGameplay() {
     return "Maju: "+maju+", Mundur: "+mundur+", Attack: "+attack+", Loncat: "+loncat;
   }
}

class Game {
  public static void main(String args[]) {
    Ingame laptopAthif = new Ingame("Kedepan tekan W", "Kebelakang tekan S", "Attack Burst dan Skill tekan Q dan E", "Loncat Untuk Terbang tekan Spasi");

    System.out.println(laptopAthif.lihatGameplay());
  }
}
Polymorphism
``````java
class Genshin {


    public void skill() {
        System.out.println("Traveler can take skill");
    }
}


class Element extends Genshin {


    @Override
    public void skill() {


        super.skill();
        System.out.println("Traveler can take skill from element ");
    }


    public void burst() {
        System.out.println("Traveler can take burst from Element");
    }
}

class Main {
    public static void main(String[] args) {


        Element labrador = new Element();


        labrador.skill();
        labrador.burst();
    }
}
